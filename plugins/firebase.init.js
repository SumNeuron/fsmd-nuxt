import firebaseConfig from '~/firebase'
import firebase from 'firebase/app'


const initFB = () => {
  if (!firebaseConfig) {
    throw new Error('missing firebase configuration file')
  }

  if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig)
  }
}

initFB()

export default initFB
