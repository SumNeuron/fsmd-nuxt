import firebase from 'firebase/app'
import initFB from '@/plugins/firebase.init.js'

initFB()

export default ({store, redirect}) => {
  if (!firebase.apps.length) {
    initFB()
  }
  return firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      store.commit('firebase/setUser', user)
    }
  })
}
