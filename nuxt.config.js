import pkg from './package'

export default {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700|Comfortaa:300,400,500,700|Material+Icons'
      }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
  // // from npx vuetify
    '~/assets/style/app.styl'
  ],

  vuetify: {
     theme: {
       primary:  '#212121',
     }
  },
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    // '@/plugins/vuetify',
    {
      src: '@/plugins/firebase.init.js',
      ssr: true
    },
    {
      src: '@/plugins/firebase.auth.js',
      ssr: true
    },
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/vuetify'
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    // from npx vuetify
    // transpile: ['vuetify/lib'],
    // plugins: [new VuetifyLoaderPlugin()],
    loaders: {
      // from npx vuetify
      stylus: {
        import: ['~assets/style/variables.styl']
      }
    },
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // config.resolve.symlinks = false
      // if (ctx.isDev) config.resolve.symlinks = false
      // if(ctx.isClient && ctx.isDev) {
      //   config.module.rules.push({
      //     enforce: 'pre',
      //     test: /\.(js|vue)$/,
      //     loader: 'eslint-loader',
      //     exclude: /(node_modules)/,
      //   });
      //   config.resolve.symlinks = false; // this prevents symlinks from breaking the build
      // }
    }
  }
}
