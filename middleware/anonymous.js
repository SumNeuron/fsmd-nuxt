import firebase from 'firebase/app'

export default ({isServer, store, redirect}) => {
  if (!isServer && (store.getters.isAuthenticated && firebase.auth().currentUser)) {
    return redirect('/user')
  }
}
