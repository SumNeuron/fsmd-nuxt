import firebase from 'firebase/app'

export default ({isServer, redirect, store, route}) => {

  if (isServer && !firebase.apps.length) {
    redirect('/user/signin')
  }

  let isSignup = route.path == '/user/signup'


  if (!isSignup && !store['firebase/isAuthenticated']) {
    redirect('/user/signin')
  }

}
