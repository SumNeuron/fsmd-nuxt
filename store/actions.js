const actions = {
  async set( {state, commit}, {k, v}) { commit('set', {k, v}) },
  async join({state, commit}, {k, v}) { commit('join', {k, v}) },
  async push({state, commit}, {k, v}) { commit('push', {k, v}) },
  async sset({state, commit}, {k, v, s}) { commit('sset', {k, v, s}) },
  async splice({state, commit}, {k, i}) { commit('splice', {k, i}) },
}


export default actions
