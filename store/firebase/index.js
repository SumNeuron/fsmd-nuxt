import Vuex from 'vuex'
import firebase from 'firebase/app';
// import 'firebase/auth';
require('firebase/auth')
require('firebase/firestore')
import {vuexfireMutations, firestoreAction} from 'vuexfire'
import {makeUserProfile, db, pRef} from '@/store/firebase/utils.js'


const getters = {
  isAuthenticated: (state) => !!state.user,
  isAnonymous: (state, getters) => getters.isAuthenticated && state.user.isAnonymous
}

const actions = {
  setProfileRef: firestoreAction(({ bindFirestoreRef }, uid) => {
    return bindFirestoreRef('profile', firebase.firestore().collection('profiles').doc(uid))
    // return bindFirestoreRef('profile', pRef().doc(uid))
  }),

  userReset ({state}) {
    state.user = null
  },
  userCreate ({state}, {email, password, username}) {
    return firebase.auth().createUserWithEmailAndPassword(email, password)
      .then(({user})=> { return makeUserProfile(user, username) })
  },
  userLogin({rootState,state, commit}, user)  {
    return firebase.auth()
      .signInWithEmailAndPassword(user.email, user.password)
      .then(({user}) => {
        return commit('setUser', user)
      })
  },
  userLogout ({state, dispatch}) {
    return firebase.auth().signOut().then(()=>dispatch('userReset'))
  },
  userUpdate ({state}, newData) {
    return firebase.database().ref(`users/${state.user.uid}`).update({
      displayName: newData.displayName
    })
  },
  userUpdateImage({state}, image) {
    return firebase.database().ref(`users/${state.user.uid}`).update({
      image
    })
  },



}

const mutations = {
  setUser(state, user) {
    state.user = user
    return this.dispatch('firebase/setProfileRef', state.user.uid)
  }
}

const state = () => ({
    user: null,
    profile: null
})

export default {
  namespaced: true,
  mutations, actions, getters, state
}
