import firebase from 'firebase/app';

export const db   = () => firebase.firestore()
export const pRef = () => db().collection('profiles')
export const sRef = () => firebase.storage().ref()


export const refLookup = {
  'profiles': pRef,
}

export const getRef = async (name) => refLookup[name]()

export const makeUserProfile = (user, username) => {
  return pRef().doc(user.uid).set({
    displayName: user.displayName || username || user.email.split('@')[0],
    email: user.email, image: user.newImage || '/images/default-profile.png',
  }).then(()=>{
    if (!user.displayName) {
      user.updateProfile({displayName: username})
    }
  })
}
